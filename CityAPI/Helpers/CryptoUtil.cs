﻿using Microsoft.SqlServer.Server;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CityAPI.Helpers
{


    public class CryptoUtil
    {
        #region CONSTANTS
        private const string SALT_VALUE = "58asdf68t7"; // can be any string
        private const int PASSWORD_ITERATIONS = 2;      // can be any number
        private const int KEY_SIZE = 256;               // can be 128, 192 or 256
        private const string HASH_ALGORITHM = "SHA1";   // can be "MD5"
        private const string PassPhrase = "knewinpass"; // can be any string
        private const string InitVector = "@5S5t5L8w3J2v5w2";         // must be 16 bytes
        #endregion CONSTANTS

        #region PRIVATE PROPERTIES

        // These members will be used to perform encryption and decryption.
        private ICryptoTransform encryptor = null;
        private ICryptoTransform decryptor = null;

        #endregion PRIVATE PROPERTIES

        private CryptoUtil Crypto { get; set; }

        #region CONSTRUCTORS
        public CryptoUtil()
        {

        }
        #endregion CONSTRUCTORS

        #region METHODS

        public CryptoUtil Init()
        {
            if (Crypto == null)
            {
                Crypto = new CryptoUtil();
            }

            SymmetricAlgorithm symmetricKey;
            symmetricKey = Aes.Create();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            byte[] passwordBytes = GeneratePassword();
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key bytes.
            encryptor = symmetricKey.CreateEncryptor(passwordBytes, initVectorBytes);

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            decryptor = symmetricKey.CreateDecryptor(passwordBytes, initVectorBytes);

            return Crypto;
        }


        private byte[] GeneratePassword()
        {
            // Convert strings into byte arrays.
            // Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            // encoding.
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(SALT_VALUE);

            // First, we must create a password, from which the key will be derived.
            // This password will be generated from the specified passphrase and 
            // salt value. The password will be created using the specified hash 
            // algorithm. Password creation can be done in several iterations.
            PasswordDeriveBytes password = new PasswordDeriveBytes(PassPhrase,
                                                            saltValueBytes,
                                                            HASH_ALGORITHM,
                                                            PASSWORD_ITERATIONS);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            return password.GetBytes(KEY_SIZE / 8);
        }

        public static string Encrypt(string pTextToEncrypt)
        {
            CryptoUtil objCrypto = new CryptoUtil();

            objCrypto.Init();

            string plainText = pTextToEncrypt;

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         objCrypto.encryptor,
                                                         CryptoStreamMode.Write);
            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            return cipherText;
        }

        public static string Decrypt(string pTextToDecrypt)
        {
            CryptoUtil objCrypto = new CryptoUtil();

            objCrypto.Init();

            string cipherText = pTextToDecrypt;

            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                          objCrypto.decryptor,
                                                          CryptoStreamMode.Read);

            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);
            return plainText;

        }

        #endregion "METHODS"
    }
}
