﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityAPI.Migrations
{
    public partial class alterQtPopulationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "QtPopulation",
                table: "City",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "QtPopulation",
                table: "City",
                type: "int",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
