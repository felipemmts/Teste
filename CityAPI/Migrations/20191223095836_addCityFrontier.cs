﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityAPI.Migrations
{
    public partial class addCityFrontier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CityFrontier",
                columns: table => new
                {
                    IdCityFrontier = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IdCity = table.Column<int>(nullable: false),
                    IdCityLimit = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CityFrontier", x => x.IdCityFrontier);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CityFrontier");
        }
    }
}
