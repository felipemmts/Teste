﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Text;

namespace CityAPI.Migrations
{
    public partial class addProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IdCityLimit",
                table: "CityFrontier",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "IdCity",
                table: "CityFrontier",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_CityFrontier_IdCity",
                table: "CityFrontier",
                column: "IdCity");

            migrationBuilder.CreateIndex(
                name: "IX_CityFrontier_IdCityLimit",
                table: "CityFrontier",
                column: "IdCityLimit");

            migrationBuilder.AddForeignKey(
                name: "FK_CityFrontier_City_IdCity",
                table: "CityFrontier",
                column: "IdCity",
                principalTable: "City",
                principalColumn: "IdCity",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CityFrontier_City_IdCityLimit",
                table: "CityFrontier",
                column: "IdCityLimit",
                principalTable: "City",
                principalColumn: "IdCity",
                onDelete: ReferentialAction.Restrict);
            
            StringBuilder storedProcedureCode = new StringBuilder();

            storedProcedureCode.Append("CREATE PROCEDURE [dbo].[SPR_GET_CITIES_FRONTIER_BY_ID_CITY]" + Environment.NewLine);
            storedProcedureCode.Append("@IdCity INT" + Environment.NewLine);
            storedProcedureCode.Append("AS" + Environment.NewLine);
            storedProcedureCode.Append("BEGIN" + Environment.NewLine);
            storedProcedureCode.Append(@"--precisa sempre cadastrar todas as cidades limites" + Environment.NewLine);
            storedProcedureCode.Append(@"SELECT * FROM CityFrontier where IdCity = @IdCity --or IdCityLimit = @IdCity" + Environment.NewLine);
            storedProcedureCode.Append("END" + Environment.NewLine);

            migrationBuilder.Sql(storedProcedureCode.ToString());

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CityFrontier_City_IdCity",
                table: "CityFrontier");

            migrationBuilder.DropForeignKey(
                name: "FK_CityFrontier_City_IdCityLimit",
                table: "CityFrontier");

            migrationBuilder.DropIndex(
                name: "IX_CityFrontier_IdCity",
                table: "CityFrontier");

            migrationBuilder.DropIndex(
                name: "IX_CityFrontier_IdCityLimit",
                table: "CityFrontier");

            migrationBuilder.AlterColumn<int>(
                name: "IdCityLimit",
                table: "CityFrontier",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "IdCity",
                table: "CityFrontier",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
