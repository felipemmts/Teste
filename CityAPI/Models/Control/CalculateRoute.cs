﻿using CityAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Models.Control
{
    public class CalculateRoute
    {
        private  List<List<int>> ways = new List<List<int>>();
        private  List<int> currentWay = new List<int>();
        private  List<int> tempWay = new List<int>();


        public List<List<int>> ShortestWay(int inicio, int final, List<CityFrontier> listCitiesFrontier)
        {

            nextCity(inicio, final, listCitiesFrontier);
            return ways.Where(w => (int)w.Last() == final).ToList();
            
        }

        private void nextCity(int inicio, int final, List<CityFrontier> listCitiesFrontier)
        {

            currentWay.Add(inicio);

            foreach (CityFrontier cityFrontier in listCitiesFrontier.Where(cf => cf.IdCity == inicio))
            {


                if (cityFrontier.IdCityLimit == final || currentWay.Contains((int)cityFrontier.IdCityLimit))
                {

                    tempWay = new List<int>();
                    tempWay.AddRange(currentWay);
                    currentWay.Add((int)cityFrontier.IdCityLimit);

                    
                    ways.Add(currentWay);

                    currentWay = new List<int>();
                    currentWay.AddRange(tempWay);
                    continue;
                }
                else
                {

                    nextCity((int)cityFrontier.IdCityLimit, final, listCitiesFrontier);
                    tempWay.Remove((int)cityFrontier.IdCityLimit);
                    currentWay.Remove((int)cityFrontier.IdCityLimit);
                }


            }

        }

    }

    
}
