﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Entities
{
    public class City
    {
        public int IdCity { get; set; }

        public string NmCity { get; set; }

        public Int64 QtPopulation { get; set; }
    }
}
