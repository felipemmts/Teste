﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Entities
{
    public class AppSettings
    {
        public string Secret { get; set; }

        public int MinutesToExpire { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int ProjectId { get; set; }
    }

}
