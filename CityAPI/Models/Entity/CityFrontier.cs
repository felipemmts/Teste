﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Entities
{
    public class CityFrontier
    {

        public int IdCityFrontier { get; set; }

        [ForeignKey("CityLimit"), Column(Order = 1)]
        public int? IdCityLimit { get; set; }

        [ForeignKey("City"), Column(Order = 0)]
        public int? IdCity { get; set; }



        public virtual City CityLimit { get; set; }

        public virtual City City { get; set; }
        


        
    }
}
