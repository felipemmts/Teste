﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Models.DTO
{
    public class NewCityDTO
    {        
        
        public Entities.City City { get; set; }

        public string[] CitiesFrontier { get; set; }


    }
}
