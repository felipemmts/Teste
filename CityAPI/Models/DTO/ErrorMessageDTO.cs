﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Models.DTO
{
    public class MessageDTO
    {
        public string Message { get; set; }
    }
}
