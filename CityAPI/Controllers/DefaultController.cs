﻿using CityAPI.Models;
using CityAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CityAPI.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    public class DefaultController : ControllerBase
    {

        protected readonly CityAPIDBContext _context = new CityAPIDBContext();
        protected readonly CityRepository _repository;
        private readonly ILogger<CityController> _logger;

        public DefaultController(IConfiguration configuration, ILogger<CityController> logger)
        {
            _repository = new CityRepository(configuration);
            _logger = logger;
        }





    }
}