﻿using CityAPI.Entities;
using CityAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CityAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TokenController : ControllerBase
    {
        private IJwtService _jwtService;
        private readonly ILogger<TokenController> _logger;

        public TokenController(IJwtService jwtService, ILogger<TokenController> logger)
        {
            _jwtService = jwtService;
            _logger = logger;
        }                                                                                                                                                                                                                                                                         

        // POST: api/Token
        /// <summary>
        /// Gerar Token.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] User user)
        {
            var userConfig = _jwtService.Authenticate(user.UserName, user.Password);

            _logger.LogInformation("TokenController - Post Authenticate: userName: {1}; password: {2}.", user.UserName, user.Password);

            if (userConfig == null)
            {
                _logger.LogInformation("TokenController - Post Authenticate: Not Authorized.");
                return BadRequest(new { message = "Not Authorized." });
            }

            _logger.LogInformation("TokenController - Post Authenticate: Authorized - Token {0}.", userConfig.Token);

            return Ok(userConfig);
        }
    }
}
