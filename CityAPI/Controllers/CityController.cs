﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityAPI.Entities;
using CityAPI.Models;
using CityAPI.Models.Control;
using CityAPI.Models.DTO;
using CityAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CityAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CityController : DefaultController
    {

        
        public CityController(IConfiguration configuration, ILogger<CityController> logger) : base(configuration,logger)
        {
           
        }


        [HttpGet]
        //[Authorize]
        public async Task<ActionResult<IEnumerable<City>>> Get()
        {

            return await _context.City.ToListAsync();
            //.ToArray();
        }

        // POST: api/Cities
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<City>> PostCity([FromBody] NewCityDTO newCityDTO)
        {
            _context.City.Add(newCityDTO.City);
            await _context.SaveChangesAsync();



            foreach (string nmCityFrontier in newCityDTO.CitiesFrontier)
            {
                City city = this.GetCityByName(nmCityFrontier);

                if (city != null)
                {
                    _context.CityFrontier.Add(new CityFrontier() { IdCity = newCityDTO.City.IdCity, IdCityLimit = city.IdCity });
                    await _context.SaveChangesAsync();

                    
                    if (!_context.CityFrontier.Any(cf => cf.IdCityLimit == newCityDTO.City.IdCity && cf.IdCity == city.IdCity))
                    {
                        _context.CityFrontier.Add(new CityFrontier() { IdCity = city.IdCity, IdCityLimit = newCityDTO.City.IdCity });
                    }

                }
            }

            return CreatedAtAction("GetCity", new { id = newCityDTO.City.IdCity }, newCityDTO.City);
        }


        // PUT: api/Cities/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult<NewCityDTO>> PutCity(int id, [FromBody] NewCityDTO newCityDTO)
        {
            if (id != newCityDTO.City.IdCity)
            {
                return BadRequest();
            }

            _context.Entry(newCityDTO.City).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            IEnumerable<CityFrontier> citiesFrontier = _repository.FindCityFrontierByIdCity(newCityDTO.City.IdCity);


            foreach (CityFrontier cityFrontier in citiesFrontier)
            {
                //var cityFrontier = await _context.CityFrontier.FindAsync(cityFrontier.IdCityFrontier);

                //if (cityFrontier == null)
                //{
                //    return NotFound();
                //}

                _context.CityFrontier.Remove(cityFrontier);
                await _context.SaveChangesAsync();

            }


            //return city;

            await AddCitiFrontier(newCityDTO);

            return newCityDTO;
        }

        private async Task AddCitiFrontier(NewCityDTO newCityDTO)
        {
            foreach (string nmCityFrontier in newCityDTO.CitiesFrontier)
            {
                City city = this.GetCityByName(nmCityFrontier);

                if (city != null)
                {
                    _context.CityFrontier.Add(new CityFrontier() { IdCity = newCityDTO.City.IdCity, IdCityLimit = city.IdCity });
                    await _context.SaveChangesAsync();
                }
            }
        }

        // GET: api/Cities/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<City>> GetCity(int id)
        {
            var city = await _context.City.FindAsync(id);

            if (city == null)
            {
                return NotFound();
            }

            return city;
        }



        // GET: api/Cities/5
        [HttpGet("search/{nameCity}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<City>>> SearchCity(string nameCity)
        {
            IQueryable<City> query = this.GetCityBySearchName(nameCity);

            return await query.ToListAsync();

        }

        private City GetCityByName(string nameCity)
        {
            return (from c in _context.City
                   where EF.Functions.Like(c.NmCity, $"{nameCity}")
                   select c).FirstOrDefault();
        }



        private IQueryable<City> GetCityBySearchName(string nameCity)
        {
            return from c in _context.City
                   where EF.Functions.Like(c.NmCity, $"%{nameCity}%")
                   select c;
        }

        [HttpPatch("{id}")]
        [Authorize]
        public async Task<IActionResult> Patch(int id, [FromBody]JsonPatchDocument patchDoc)
        {


            if (id <= 0 || patchDoc == null) 
                return BadRequest();

            if (patchDoc.Operations.Where(o => o.path.Contains("IdCity")).Count() > 0)
            {
                return BadRequest();
            }

            if (patchDoc != null)
            {
                var city = await _context.City.FindAsync(id);

                patchDoc.ApplyTo(city);


                _context.Entry(city).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CityExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return CreatedAtAction("GetCity", new { id = city.IdCity }, city);
            }
            else
            {
                return BadRequest();
            }
        }

        private bool CityExists(int id)
        {
            return _context.City.Any(e => e.IdCity == id);
        }

        [HttpPost("SumCitiesPopulationByNames")]
        [Authorize]
        public async Task<ActionResult<long>> SumCitiesPopulationByNames([FromBody] IEnumerable<string> cities)
        {


            long total = await Task<long>.Run(()=>(from c in _context.City
                          where cities.Contains(c.NmCity)
                          select c).Sum(x => x.QtPopulation));


            return total;


        }

        [HttpGet("WhoDoesFrontierById/{id}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<City>>> WhoDoesFrontierById(int id)
        {
            IEnumerable<CityFrontier> citiesFrontier = _repository.FindCityFrontierByIdCity(id);
            List<City> cities = new List<City>();
            foreach (CityFrontier cityFrontier in citiesFrontier)
            {

                if (cityFrontier.IdCity == id)
                {
                    cities.Add(await _context.City.FindAsync(cityFrontier.IdCityLimit));
                }
                else
                {
                    cities.Add(await _context.City.FindAsync(cityFrontier.IdCityLimit));
                }

            }

            return cities;

        }

        [HttpGet("WhoDoesFrontierByName/{name}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<City>>> WhoDoesFrontierByName(string name)
        {

            City city = GetCityByName(name);

            if (city != null)
            {

                IEnumerable<CityFrontier> citiesFrontier = _repository.FindCityFrontierByIdCity(city.IdCity);
                List<City> cities = new List<City>();
                foreach (CityFrontier cityFrontier in citiesFrontier)
                {

                    if (cityFrontier.IdCity == city.IdCity)
                    {
                        cities.Add(await _context.City.FindAsync(cityFrontier.IdCityLimit));
                    }
                    else
                    {
                        cities.Add(await _context.City.FindAsync(cityFrontier.IdCityLimit));
                    }



                    return cities;
                }
            }

            return BadRequest();

        }

        [HttpGet("GetRouteById/{idStart}/to/{idFinish}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<IEnumerable<City>>>> GetRouteById(int idStart, int idFinish)
        {

            City cityStart = await _context.City.FindAsync(idStart),
                 cityFinish = await _context.City.FindAsync(idFinish);


            CalculateRoute calculateRoute = new CalculateRoute();

            List<List<int>> listShortestWay = calculateRoute.ShortestWay(idStart, idFinish, _context.CityFrontier.ToList());

            List<List<City>> listListCity = new List<List<City>>();

            foreach (List<int> list in listShortestWay.OrderBy(l => l.Count()))
            {

                if (listListCity.Count() > 0 && list.Count() > listListCity.Last().Count)
                {
                    //caso tenha rotas maiores não precisa acrescnetar, só interessa as melhores
                    break;
                }
                
                
                List<City> listCity = new List<City>();

                foreach (int idCity in list)
                {

                    listCity.Add(_context.City.Find(idCity));

                }

                listListCity.Add(listCity);

            }

            return listListCity;

        }



    }
}
