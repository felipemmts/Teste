﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CityAPI.Entities;


namespace CityAPI.Configuration
{
    public class CityEntityConfiguration : IEntityTypeConfiguration<CityAPI.Entities.City>
    {

        public void Configure(EntityTypeBuilder<CityAPI.Entities.City> builder)
        {
            builder.Property(q => q.NmCity).HasMaxLength(255);

            builder.ToTable("City").HasKey(q => q.IdCity);

        }
    }
}