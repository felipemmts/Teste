﻿using CityAPI.Helpers;
using CityAPI.Singleton;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;

namespace CityAPI.Models
{
    public class CityAPIDBContext : DbContext
    {

        public DbSet<Entities.City> City { get; set; }

        public DbSet<Entities.CityFrontier> CityFrontier { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(CityAPISingleton.Instance.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }


    }


}
