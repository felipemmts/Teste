﻿using CityAPI.Helpers;
using Microsoft.Extensions.Configuration;
using System;

namespace CityAPI.Repositories
{
    public abstract class AbstractRepository
    {
        private string _connectionString;
        protected string ConnectionString => _connectionString;
        public AbstractRepository(IConfiguration configuration)
        {
            _connectionString = Singleton.CityAPISingleton.Instance.ConnectionString;
        }             
    }
}
