﻿using CityAPI.Singleton;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CityAPI.Constants;

namespace CityAPI.Repositories
{
    public class CityRepository : AbstractRepository
    {
        public CityRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<Entities.CityFrontier> FindCityFrontierByIdCity(int IdCity)
        {
            using (IDbConnection dbConnection = new SqlConnection(CityAPISingleton.Instance.ConnectionString))
            {
                dbConnection.Open();
                return dbConnection.Query<CityAPI.Entities.CityFrontier>(
                    Constant.SPR_GET_CITIES_FRONTIER_BY_ID_CITY,
                    new { IdCity = IdCity },
                    commandType: CommandType.StoredProcedure); 
            }
        }
    }
}
