﻿using CityAPI.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace CityAPI.Services
{
    public interface IJwtService
    {
        User Authenticate(string username, string password);
        //IEnumerable<User> GetAll();
    }

    public class JwtService : IJwtService
    {
        private User _user;
        
        private readonly AppSettings _appSettings;

        public JwtService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _user = new User { UserName = _appSettings.UserName, Password = _appSettings.Password };
        }

        public User Authenticate(string username, string password)
        {
            if (!_user.UserName.Equals(username) && !_user.Password.Equals(password))
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {               
                Expires = DateTime.UtcNow.AddMinutes(_appSettings.MinutesToExpire),
                NotBefore = DateTime.UtcNow,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            _user.Token = tokenHandler.WriteToken(token);
            
            _user.Password = null;

            return _user;
        }      
    }
}
