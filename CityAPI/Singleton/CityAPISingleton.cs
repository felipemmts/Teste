﻿using CityAPI.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CityAPI.Singleton
{
    public sealed class CityAPISingleton
    {
        static CityAPISingleton _instance;
        private string _connectionString;
        public static CityAPISingleton Instance
        {
            get { return _instance ?? (_instance = new CityAPISingleton()); }
        }
        private CityAPISingleton()
        {
            IConfigurationRoot configuration = new  ConfigurationBuilder()
                                                    .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                                                    .AddJsonFile("appsettings.json")
                                                    .Build();

            _connectionString =  String.Format(configuration.GetConnectionString("DefaultConnection"),
                                    CryptoUtil.Decrypt(configuration.GetConnectionString("db.user")),
                                    CryptoUtil.Decrypt(configuration.GetConnectionString("db.password")));
        }
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
        }
    }
}
