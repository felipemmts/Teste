﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CityAPI.Extensions
{
    public static class SwaggerServiceExtension
    {
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, IConfiguration configuration)
        {

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            //});

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "City API",
                    Description = "A simple example ASP.NET Core Web API"

                });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                //OpenApiSecurityRequirement open = new OpenApiSecurityRequirement();

                //c.OperationFilter<SecurityRequirementsOperationFilter>();

                // Set the comments path for the Swagger JSON and UI.
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            
        });

            return services;
        }

        /// <summary>
        /// Uses the swagger documentation.
        /// </summary>
        /// <returns>The swagger documentation.</returns>
        /// <param name="app">App.</param>
        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app, IConfiguration configuration)
        {
            app.UseSwagger(
                c => c.RouteTemplate = "swagger/{documentName}/swagger.json"
            );

            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint(string.Format("{0}/swagger.json", configuration["Swagger:SwaggerVersion"]), "City API - Martins");
                s.RoutePrefix = "swagger";
                s.DocumentTitle = "City API - Martins";
                s.DocExpansion(DocExpansion.None);
            });

            return app;
        }
    }
}
